package Models;

public interface INode {
	
	public int getX();
	public int getY();
}
